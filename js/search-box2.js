var animeData = [
    { name: "The Huntsman: Winter’s War", linkWatch: "movie-detail-winter.html", picture: "img/the-huntsman-winter-war.jpg" },
    { name: "The Legend of Tarzan", linkWatch: "movie-detail-legend-tarzan.html", picture: "img/legend-tarzan.jpg" },
    { name: "ไอฟาย..แต๊งกิ้ว..เลิฟยู้", linkWatch: "movie-detail-thanks-love-you.html", picture: "img/I_FINE.jpg" },
    { name: "มอนโด รัก โพสต์ ลบ ลืม", linkWatch: "movie-detail-mondo-love-post.html", picture: "img/mon-do.jpg" },
    { name: "นะหน้าทอง (Black Magic Mask)", linkWatch: "movie-detail-black-magic-mask.html", picture: "img/glod-face.jpg" },
    { name: "แมนสรวง", linkWatch: "movie-detail-man-sruang.html", picture: "img/man-suang.jpg" },
    { name: "เสือเผ่น ๑", linkWatch: "movie-detail-tiger-run.html", picture: "img/tigle-run.jpg" },
    { name: "ฉลาดเกมส์โกง", linkWatch: "movie-detail-genius-game.html", picture: "img/genius-game.jpg" },
    { name: "กวน มึน โฮ", linkWatch: "movie-detail-guan-mun-hho.html", picture: "img/guan-man-hoo.jpg" }
];


const searchBox = document.getElementById('search-box');
const searchButton = document.getElementById('search-btn');
const searchContainer = document.getElementById('search-results');
const resultsBox = document.querySelector(".result-box")


searchBox.onkeyup = function(){
    let result = [];
    let input = searchBox.value.toLowerCase();
    console.log("input",input);
    for (var i = 0; i < animeData.length; i++){
        if  (input.length){
            if (animeData[i].name.toLowerCase().includes(input)) {
                result.push(animeData[i].name);
            }
        }else if(!input.length){
            result.innerHTML = '';
        }
    }
    
    console.log('result',result);
    display(result);
    
}

function display(result){
    const content = result.map((list)=>{
        return "<li onclick=selectInput(this)>" + list + "</li>";
    });

    resultsBox.innerHTML = "<ul>" + content.join('') + "</ul>";
}

function selectInput(list){
    searchBox.value = list.innerHTML;
    resultsBox.innerHTML = '';
}


searchButton.addEventListener("click", function(){
    var searchTerm = searchBox.value.toLowerCase();
    var searchResults = [];

    for (var i = 0; i < animeData.length; i++) {
        if (animeData[i].name.toLowerCase().includes(searchTerm)) {
            searchResults.push(animeData[i]);
        }
    }
    console.log("searchResults", searchResults);
    searchContainer.innerHTML = "";
    searchResults.forEach(function(anime) {
        var resultLink = document.createElement("a");
        // resultLink.textContent = anime.name;
        resultLink.href = anime.linkWatch;

        var resultImg = document.createElement("img");
        
        resultImg.src = searchResults[0].picture;
        console.log("resultImg", resultImg);
        resultImg.style.width = "80%";

        resultLink.appendChild(resultImg);
        // resultLink.classList.add("result-link");
        searchContainer.appendChild(resultLink);
    })
});