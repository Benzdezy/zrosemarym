var animeData = [
    { name: "100 คำถามอะไรเอ่ย ฉบับ ท้าให้ทาย กระจายมุกป่วน", linkWatch: "book-detail-100-question.html", picture: "img/100-question.jpg" },
    { name: "คดีฆาตกรรมโดมปรมาณู", linkWatch: "book-detail-dom-boon.html", picture: "img/dom-boom.jpg" },
    { name: "ภาพวาดปริศนากับการตามหาฆาตกร", linkWatch: "book-detail-drawing-pluzzle.html", picture: "img/drawing-pluzzle.jpg" },
    { name: "คดีฆาตกรรมภูเขาไฟฟูจิ", linkWatch: "book-detail-fuji.html", picture: "img/fuji.jpg" },
    { name: "คดีฆาตกรรมวัดปราสาททอง", linkWatch: "book-detail-glod-casle.html", picture: "img/glod-casle.jpg" },
    { name: "คดีฆาตกรรมในบ้านสิบเหลี่ยม", linkWatch: "book-detail-house.html", picture: "img/house.png" },
    { name: "ฆาตกรมนุษย์กบกับศพปริศนา", linkWatch: "book-detail-human-fog.html", picture: "img/human-fog.jpg" },
    { name: "ร้านไม่สะดวกซื้อของคุณทกโก", linkWatch: "book-detail-market.html", picture: "img/market.jpg" },
    { name: "บ้านวิกลคนประหลาด", linkWatch: "book-detail-monster-home.html", picture: "img/monster-home.jpg" },
    { name: "มุมมองนักอ่านพระเจ้า เล่ม 1", linkWatch: "book-detail-read-view-of-god.html", picture: "img/read-view-of-god.jpg" },
    { name: "ฆาตกรรมปิดตายบนภูเขาหิมะ", linkWatch: "book-detail-room-close-mountain.html", picture: "img/room-close-mountain.jpg" },
    { name: "คดีฆาตกรรมวัดปราสาทเงิน", linkWatch: "book-detail-silver-casle.html", picture: "img/silver-casle.jpg" }
];


const searchBox = document.getElementById('search-box');
const searchButton = document.getElementById('search-btn');
const searchContainer = document.getElementById('search-results');
const resultsBox = document.querySelector(".result-box")


searchBox.onkeyup = function(){
    let result = [];
    let input = searchBox.value.toLowerCase();
    console.log("input",input);
    for (var i = 0; i < animeData.length; i++){
        if  (input.length){
            if (animeData[i].name.toLowerCase().includes(input)) {
                result.push(animeData[i].name);
            }
        }else if(!input.length){
            result.innerHTML = '';
        }
    }
    
    console.log('result',result);
    display(result);
    
}

function display(result){
    const content = result.map((list)=>{
        return "<li onclick=selectInput(this)>" + list + "</li>";
    });

    resultsBox.innerHTML = "<ul>" + content.join('') + "</ul>";
}

function selectInput(list){
    searchBox.value = list.innerHTML;
    resultsBox.innerHTML = '';
}


searchButton.addEventListener("click", function(){
    var searchTerm = searchBox.value.toLowerCase();
    var searchResults = [];

    for (var i = 0; i < animeData.length; i++) {
        if (animeData[i].name.toLowerCase().includes(searchTerm)) {
            searchResults.push(animeData[i]);
        }
    }
    console.log("searchResults", searchResults);
    searchContainer.innerHTML = "";
    searchResults.forEach(function(anime) {
        var resultLink = document.createElement("a");
        // resultLink.textContent = anime.name;
        resultLink.href = anime.linkWatch;

        var resultImg = document.createElement("img");
        
        resultImg.src = searchResults[0].picture;
        console.log("resultImg", resultImg);
        resultImg.style.width = "80%";

        resultLink.appendChild(resultImg);
        // resultLink.classList.add("result-link");
        searchContainer.appendChild(resultLink);
    })
});