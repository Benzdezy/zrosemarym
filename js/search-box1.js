var animeData = [
    { name: "Jujutsu Kaisen", linkWatch: "video-detail-jujutsu-kaisen.html", picture: "img/jujutsu-kaisen.jpg" },
    { name: "Record of Ragnarok", linkWatch: "video-detail-record-of-ragnarok.html", picture: "img/record-of-ragnarok.jpg" },
    { name: "Hunter X Hunter", linkWatch: "video-detail-hunter-x-hunter.html", picture: "img/hunter-x-hunter.jpg" },
    { name: "Detective Conan The Movie 26", linkWatch: "video-detail-detective-conan-the-movie-26.html", picture: "img/detective-conan-the-movie-26.jpg" },
    { name: "Doraemon The Movie 2023 Nobita's Sky Utopia", linkWatch: "video-detail-doraemon-the-movie-2023.html", picture: "img/doraemon-the-movie-2023.jpg" },
    { name: "Kimetsu no Yaiba Swordsmith Village", linkWatch: "video-detail-kimetsu-no-yaiba.html", picture: "img/kimetsu-no-yaiba-swordsmith-village.jpg" },
    { name: "SPY x FAMILY", linkWatch: "video-detail-spy-x-family.html", picture: "img/spy-x-family.jpg" },
    { name: "The Promised Neverland", linkWatch: "video-detail-the-promised-neverland.html", picture: "img/the-promised-neverland.jpg" },
    { name: "Oshi no ko", linkWatch: "video-detail-oshi-no-ko.html", picture: "img/oshi-no-ko.jpg" },
    { name: "My Neighbour Totoro", linkWatch: "video-detail-my-neighbour-totoro.html", picture: "img/my-neighbour-totoro.jpg" },
    { name: "Spirited Away", linkWatch: "video-detail-spirited-away.html", picture: "img/spirited-away.jpg" },
    { name: "Inuyasha", linkWatch: "video-detail-inuyasha.html", picture: "img/inuyasha.jpg" }
];


const searchBox = document.getElementById('search-box');
const searchButton = document.getElementById('search-btn');
const searchContainer = document.getElementById('search-results');
const resultsBox = document.querySelector(".result-box")


searchBox.onkeyup = function(){
    let result = [];
    let input = searchBox.value.toLowerCase();
    console.log("input",input);
    for (var i = 0; i < animeData.length; i++){
        if  (input.length){
            if (animeData[i].name.toLowerCase().includes(input)) {
                result.push(animeData[i].name);
            }
        }else if(!input.length){
            result.innerHTML = '';
        }
    }
    
    console.log('result',result);
    display(result);
    
}

function display(result){
    const content = result.map((list)=>{
        return "<li onclick=selectInput(this)>" + list + "</li>";
    });

    resultsBox.innerHTML = "<ul>" + content.join('') + "</ul>";
}

function selectInput(list){
    searchBox.value = list.innerHTML;
    resultsBox.innerHTML = '';
}


searchButton.addEventListener("click", function(){
    var searchTerm = searchBox.value.toLowerCase();
    var searchResults = [];

    for (var i = 0; i < animeData.length; i++) {
        if (animeData[i].name.toLowerCase().includes(searchTerm)) {
            searchResults.push(animeData[i]);
        }
    }
    console.log("searchResults", searchResults);
    searchContainer.innerHTML = "";
    searchResults.forEach(function(anime) {
        var resultLink = document.createElement("a");
        // resultLink.textContent = anime.name;
        resultLink.href = anime.linkWatch;

        var resultImg = document.createElement("img");
        
        resultImg.src = searchResults[0].picture;
        console.log("resultImg", resultImg);
        resultImg.style.width = "80%";

        resultLink.appendChild(resultImg);
        // resultLink.classList.add("result-link");
        searchContainer.appendChild(resultLink);
    })
});